package gmailCheckDeleteTest;

import gmailCheckDeleteTest.businessObjects.GmailBusinessObject;
import gmailCheckDeleteTest.businessObjects.LoginBusinessObject;
import gmailCheckDeleteTest.components.DriverPool;
import gmailCheckDeleteTest.components.parsers.PropertyParser;
import gmailCheckDeleteTest.model.Users;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class GmailMarkDeleteOperations {

    private Users users;
    private PropertyParser propertyParser;
    private DriverPool driverPool;

    @BeforeClass
    public void driverSetting() {
        propertyParser = new PropertyParser("src/test/resources/config.properties");
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
    }

    @Test(dataProvider = "user-data")
    public void gmailEnterTest(String login, String password) {

        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://www.gmail.com/");
        LoginBusinessObject loginBusinessObject = new LoginBusinessObject(driver);
        loginBusinessObject.logIn(login, password);

        GmailBusinessObject gmailBusinessObject = new GmailBusinessObject(driver);
        gmailBusinessObject.emailImportant();
        gmailBusinessObject.emailCheckBox();
        gmailBusinessObject.emailDelete();

        Assert.assertTrue(gmailBusinessObject.isMessagesDelete());
        driver.quit();
    }

    @AfterClass
    public void driversQuit() {
        driverPool.quitAll();
    }

    @DataProvider(name = "user-data", parallel = true)
    public static Object[][] users(){
        return new Object[][]{
                {"autohelp.lviv@gmail.com","13101998gmail"},
                {"ta.lab.testprofile@gmail.com","ownerofthisPROFILE"},
                {"vfemyaktest@gmail.com","test1234test"}
        };
    }
}