import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;
import java.util.concurrent.TimeUnit;

public class GmailMarkDeleteOperations {
    WebDriver driver;
    private String mail = "autohelp.lviv@gmail.com";
    private String password = "13101998gmail";

    @BeforeClass
    public void driverSetting() {
        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Test
    public void gmailEnterTest() {
        driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://www.gmail.com/");

        GmailPageObject gmailPageObject = new GmailPageObject(driver);
        gmailPageObject.loginAndNext(mail);
        new WebDriverWait(driver, 35).until(dr -> dr.findElement(By.id("profileIdentifier")).getText().equalsIgnoreCase(mail));
        gmailPageObject.passwordAndNext(password);
        new WebDriverWait(driver, 35).until(dr -> dr.findElement(By.xpath("//img[@class='gb_0a']")).isDisplayed());
        gmailPageObject.checkImportans();
        new WebDriverWait(driver, 35).until(dr -> dr.findElement(By.xpath("//span[@class='aT']//span[@class='bAq']")).isDisplayed());
        gmailPageObject.myWait(driver);
        gmailPageObject.checkBox();
        new WebDriverWait(driver, 35).until(dr -> dr.findElement(By.xpath("//div[@role='button' and @act='10']")).isDisplayed());

        gmailPageObject.deleteMessage();
    }

    @AfterClass
    public void driverQuit() {
        //driver.quit();
    }
}