import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;

public class GmailPageObject {

    @FindBy(name = "identifier")
    private WebElement elementMail;

    @FindBy(id = "identifierNext")
    private WebElement buttonNext;

    @FindBy(name = "password")
    private WebElement elementPassword;

    @FindBy(id = "passwordNext")
    private WebElement buttonPasswordNext;

    @FindBy(xpath = "//div[@class='pG' and @role='img']")
    private List<WebElement> importants;

    @FindBy(xpath = "//div[@id=':2r' and @role='button']")
    private WebElement more;

    @FindBy(xpath = "//div[@role='menuitem']//div[@class='J-N-Jz']")
    private WebElement important;

    @FindBy(xpath = "//td[@class = 'oZ-x3 xY']//div[@role='checkbox']")
    private List<WebElement> checkBoxs;

    @FindBy(xpath = "//div[@role='button' and @act='10']")
    private WebElement buttonDelete;

    @FindBy(xpath = "//span[@class='aT']//span[@class='bAq']")
    private WebElement mgIsCheck;

    public GmailPageObject(WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    public void loginAndNext(String login){
        elementMail.sendKeys(login);
        buttonNext.click();
    }
    public void passwordAndNext(String password){
        elementPassword.sendKeys(password);
        buttonPasswordNext.click();
    }
    public void checkImportans(){
        for(int i=0; i<3; i++)
        {
            importants.get(i).click();
        }
    }
    public void checkBox(){
        for(int i=0; i<3; i++)
        {
            checkBoxs.get(i).click();
        }
    }
    public void myWait(WebDriver driver){
        while (!mgIsCheck.isDisplayed()){
            new WebDriverWait(driver, 35);
        }
    }
    public void deleteMessage(){
        buttonDelete.click();
        //LOG.info("Delete elements");
    }
}