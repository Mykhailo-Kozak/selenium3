import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class GmailElementObject {

    @FindBy(name = "identifier")
    private WebElement elementMail;

    @FindBy(id = "identifierNext")
    private WebElement buttonNext;

    @FindBy(name = "password")
    private WebElement elementPassword;

    @FindBy(id = "passwordNext")
    private WebElement buttonPasswordNext;

    @FindBy(xpath = "//td[@class = 'WA xY']//div[@class='pG']")
    private List<WebElement> importants;

    @FindBy(xpath = "//td[@class = 'oZ-x3 xY']//div[@role='checkbox']")
    private List<WebElement> checkBoxs;

    @FindBy(xpath = "//div[@role='button' and @act='10']")
    private WebElement buttonDelete;

    public GmailElementObject(WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    public void loginAndNext(String login){
        elementMail.sendKeys(login);
        buttonNext.click();
    }
    public void passwordAndNext(String password){
        elementPassword.sendKeys(password);
        buttonPasswordNext.click();
    }
    public void checkImportans(){
        for(int i=0; i<3; i++)
        {
            importants.get(i).click();
        }
    }
    public void checkBox(){
        for(int i=0; i<3; i++)
        {
            checkBoxs.get(i).click();
        }
    }
    public void deleteMessage(){
        buttonDelete.click();
    }
}